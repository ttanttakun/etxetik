#!/bin/sh

# rm link (if already exists) 
rm /home/headless/ctl/public/downloads

ln -s /data/downloads /home/headless/ctl/public/downloads
qjackctl -p dummy -a /scripts/patchbay.xml --start &
sleep 10

## debug
# firefox --profile ~/firefox_profile https://meet.jit.si/ttanttakuntest1 &

liquidsoap /scripts/create_stream.liq &
liquidsoap /scripts/music.liq &
pm2 start ~/ctl/index.js