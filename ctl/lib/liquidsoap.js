const Telnet = require("telnet-client")

let isConnected = false

function _sendCommand(cmd) {
  return new Promise(async (resolve, reject) => {
    let connection = new Telnet()

    let params = {
      host: "localhost",
      port: 2424,
      negotiationMandatory: false,
      timeout: 5000,
    }

    try {
      await connection.connect(params)
      isConnected = true
    } catch (error) {
      return reject(error)
    }

    try {
      let res = await connection.send(cmd)
      resolve(res)
    } catch (error) {
      reject(error)
    }
  })
}

module.exports = {
  enqueue(filepath) {
    return _sendCommand(`request.push file://${filepath}`)
  },
  skip() {
    return _sendCommand(`music.skip`)
  },
  ignore(rid) {
    return _sendCommand(`request.ignore ${rid}`)
  },
  setVolume(vol) {
    return _sendCommand(`var.set musicvol=${parseFloat(vol).toFixed(1)}`)
  },
  getVolume() {
    return _sendCommand(`var.get musicvol`)
  },
  metadata() {
    return _sendCommand(`music.metadata`)
  },
  remaining() {
    return _sendCommand(`music.remaining`)
  },
  stop() {
    return _sendCommand(`music.stop`)
  },
  start() {
    return _sendCommand(`music.start`)
  },
}
