const {spawn, exec} = require('child_process')

const profilePath = process.env['FIREFOX_PROFILE_PATH'] || '/tmp/etxetik_firefox_profile'

module.exports = {
    open(url) {
        return new Promise((resolve, reject) => {
            spawn('firefox', ['--profile', profilePath, url], {
                stdio: 'ignore',
                detached: true,
            })
            resolve()
        })
    },
    close() {
        return new Promise((resolve, reject) => {
            exec('killall firefox', function(error, stdout, stderr) {
                if (error) {
                    return reject(error)
                }
                resolve()
            })
        })
    }
}
