const path = require("path")
const { spawn } = require('child_process')

module.exports = function (url, downloadDir = '/tmp') {
    return new Promise(async (resolve, reject) => {
      const dl = spawn('youtube-dl', ['-x', '--audio-format=mp3', `-o%(title)s-%(id)s.%(ext)s`, '--print-json', url], {
        cwd: downloadDir,
      })
  
      let _err = []
      let _data = []
  
      dl.stdout.on('data', (data) => {
        _data.push(data.toString())
      })
      
      dl.stderr.on('data', (data) => {
        _err.push(data.toString())
      })
      
      dl.on('close', (code) => {
        if (code) {
          return reject(new Error(_err.join()))
        }
        // TODO: wait to flush? this is so wrong and it fails sometimes
        setTimeout(() => {
          try {
            let data = JSON.parse(_data.join())
            let filename = data._filename
            filename = filename.split('.')
            filename.pop()
            filename = filename.join('.') + '.mp3'
            filename = path.join(downloadDir, filename)
            resolve(filename)
          } catch (error) {
            reject(_err.join())
          }
        }, 100)
      })
    })
  }