const fs = require("fs")

module.exports = {
  ls(dir = "/tmp") {

    return new Promise((resolve, reject) => {
      fs.readdir(dir, { withFileTypes: true }, (err, _files) => {
        if (err) return reject(err)

        let files = []
        _files.forEach((dirent) => {
          files.push({ name: dirent.name, isDir: !dirent.isFile() })
        })

        return resolve(files)
      })
    })
  },
  rename(oldPath, newPath) {
    return new Promise((resolve, reject) => {
      fs.rename(oldPath, newPath, function (error) {
        if (error) return reject(error)

        resolve()
      })
    })
  },
  remove(filepath) {
    return new Promise((resolve, reject) => {
      fs.unlink(filepath, function (error) {
        if (error) return reject(error)

        resolve()
      })
    })    
  }
}
