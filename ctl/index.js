const path = require("path")

const download = require("./lib/youtube-dl")
const music = require("./lib/liquidsoap")
const firefox = require("./lib/firefox")
const utils = require("./lib/utils")

const DOWNLOADS_DIR = process.env.DOWNLOADS_DIR || "/tmp/"
const PASSWORD = process.env.CTL_PW || "password"
const JITSI_URL = process.env.JITSI_URL || "https://meet.jit.si"

const fastify = require("fastify")({
  logger: true,
})

const io = require("socket.io")(fastify.server)

fastify.register(require("fastify-static"), {
  root: path.join(__dirname, "public"),
  prefix: "/",
})

const talkOverVolume = 0.1
let sockets = {}
let downloadQueue = []
let _volume = 1.0
let _remaining = 0
let played = []
let _lastVolume = 1.0
let isTalkOver = false

let state = {
  room: "",
  queue: [],
  isDownloading: false,
  currentMetadata: {},
  roomOpen: false,
  playing: true,
}

function getState() {
  return {
    ...state,
    downloadQueue: downloadQueue.length,
  }
}

function broadcast(cmd, data) {
  Object.values(sockets).forEach((socket) => {
    socket.emit(cmd, data)
  })
}

function broadcastState() {
  broadcast("state", getState())
}

async function processDownloads() {
  if (state.isDownloading || !downloadQueue.length) return

  let url = downloadQueue.shift()

  state.isDownloading = true
  broadcastState()

  let filepath
  try {
    filepath = await download(url, DOWNLOADS_DIR)
  } catch (error) {
    broadcast("failed", {
      type: "download",
      error: `Failed to download "${url}"`,
      trace: error,
      date: new Date(),
    })
  }

  state.isDownloading = false
  broadcastState()

  let files = await utils.ls(DOWNLOADS_DIR)
  broadcast("list", files)

  processDownloads()
}

function getCurrentMetadata() {
  return new Promise(async (resolve, reject) => {
    try {
      let meta = await music.metadata()
      let last = meta
        .split(/---\s\d\s---/gm)
        .pop()
        .replace("END", "")
        .replace("\r", "")
        .replace("\r", "")
        .split("\n")
        .filter((line) => line.length)
      let o = {}
      last.forEach((line) => {
        let l = line.split("=")
        if (l.length > 1) {
          try {
            o[l.shift()] = JSON.parse(l.join("="))
          } catch (error) {
            console.error("getCurrentMetadata: couldnt parse", l)
          }
        }
      })
      resolve(o)
    } catch (error) {
      reject(error)
    }
  })
}

function getRemaining() {
  return new Promise(async (resolve, reject) => {
    try {
      let remaining = await music.remaining()
      remaining = parseFloat(remaining.replace("\r\nEND\r\n", ""))
      resolve(remaining)
    } catch (error) {
      reject(error)
    }
  })
}

function getVolume() {
  return new Promise(async (resolve, reject) => {
    let vol = await music.getVolume()
    resolve(parseFloat(vol.replace("\r\nEND\r\n"), ""))
  })
}

setInterval(() => {
  _metadataInterval()
  _remainingInterval()
}, 2000)

async function _metadataInterval() {
  try {
    let meta = await getCurrentMetadata()

    if (state.currentMetadata.on_air != meta.on_air) {
      lastMeta = meta
      state.currentMetadata = {
        on_air: meta.on_air,
        filename: meta.filename.replace(DOWNLOADS_DIR, "").replace("/", ""),
        filepath: meta.filename,
      }
      if (
        state.queue.length &&
        state.currentMetadata.filepath === state.queue[0].filepath
      ) {
        state.queue.shift()
        played.push(state.currentMetadata)
        broadcast("played", played)
      }
      broadcastState()
    }
  } catch (error) {
    console.error("metadatainterval:error", error.toString())
  }
}

async function _remainingInterval() {
  try {
    let d = Date.now()
    let remaining = await getRemaining()
    _remaining = remaining
    broadcast("remaining", {remaining, d})
  } catch (error) {
    console.error("remaininginterval:error", error.toString())
  }
}

io.on("connection", function (socket) {
  console.log("connected", socket.id)
  let logged = false

  socket.on("login", function (pass) {
    if (pass !== PASSWORD) {
      return socket.disconnect(true)
    }
    logged = true
    sockets[socket.id] = socket
    // send initial state
    socket.emit("logged", { logged: true })
    socket.emit("volume", _volume)
    socket.emit("remaining", _remaining)
    socket.emit("state", getState())
    socket.emit("played", played)
    socket.emit("talkover", isTalkOver)
    addListeners(socket)
  })
})

function addListeners(socket) {
  socket.on("list", async function (dir_path = "") {
    let files = await utils.ls(path.join(DOWNLOADS_DIR, dir_path))
    socket.emit("list", files)
  })

  socket.on("state", async function () {
    socket.emit("state", getState())
  })

  socket.on("download", async function (url) {
    if (!url) return
    downloadQueue.push(url)
    broadcastState()
    processDownloads()
  })

  socket.on("enqueue", async function (filepath) {
    filepath = path.join(DOWNLOADS_DIR, filepath)
    let rid = await music.enqueue(filepath)
    rid = rid.replace("\r\nEND\r\n", "")

    if (rid) {
      state.queue.push({
        filepath,
        rid,
        filename: filepath.replace(DOWNLOADS_DIR, "").replace("/", ""),
        date: new Date(),
      })
      broadcastState()
    }
  })

  socket.on("skip", async function () {
    await music.skip()
    broadcastState()
  })

  socket.on("ignore", async function (rid) {
    try {
      await music.ignore(rid)

      let i = state.queue.findIndex((item) => {
        return item.rid == rid
      })

      state.queue.splice(i, 1)

      broadcastState()
    } catch (error) {
      console.error("ignore", error.toString())
    }
  })

  socket.on("volume", async function (volume) {
    try {
      _volume = parseFloat(volume).toFixed(2)
      broadcast("volume", _volume)
      await music.setVolume(volume)
    } catch (error) {
      console.error("volume", error.toString())
    }
  })

  socket.on("talkover", async function (on) {
    switch (on) {
      case true:
        if (!isTalkOver) {
          isTalkOver = true
          _lastVolume = parseFloat(_volume)
          _volume = talkOverVolume.toFixed(2)
          // dont wait for answer
          music.setVolume(_volume)
          broadcast("volume", _volume)
          broadcast("talkover", isTalkOver)
        }
        break

      case false:
        if (isTalkOver) {
          isTalkOver = false
          _volume = _lastVolume
          // dont wait for answer
          music.setVolume(_volume)
          broadcast("volume", _volume)
          broadcast("talkover", isTalkOver)
        }
        break
    }
  })

  socket.on("firefox:open", async function (room) {
    state.room = `${JITSI_URL}/${room}`
    try {
      await firefox.close()
      state.roomOpen = false
      broadcastState()
    } catch (error) {
      console.error("firefox:open", error.toString())
    }

    try {
      // wait a bit after firefox closes
      setTimeout(async () => {
        await firefox.open(state.room)
        state.roomOpen = true
        broadcastState()
      }, 1000)
    } catch (error) {
      console.error("firefox:open", error.toString())
    }
  })

  socket.on("firefox:close", async function () {
    try {
      await firefox.close()
      state.roomOpen = false
      broadcastState()
    } catch (error) {
      console.error("firefox:close", error.toString())
    }
  })

  socket.on("pause", async function () {
    try {
      await music.stop()
      state.playing = false
      broadcastState()
    } catch (error) {
      console.error("music:pause", error.toString())
    }
  })

  socket.on("play", async function () {
    try {
      await music.start()
      state.playing = true
      broadcastState()
    } catch (error) {
      console.error("music:play", error.toString())
    }
  })

  socket.on("clearplayed", async function () {
    played = []
    broadcast("played", played)
  })

  socket.on("rename", async function (data) {
    try {
      await utils.rename(path.join(DOWNLOADS_DIR, data.old), path.join(DOWNLOADS_DIR, data.new))
      let dir = data.old.split('/')
      dir.pop()
      let files = await utils.ls(path.join(DOWNLOADS_DIR, dir.join('/')))
      broadcast("list", files)
    } catch(error) {
      broadcast("failed", {
        type: "rename",
        error: `Failed to rename "${data.old}" to "${data.new}"`,
        trace: error,
        date: new Date(),
      })
    }
  })

  socket.on("remove", async function (data) {
    try {
      await utils.remove(path.join(DOWNLOADS_DIR, data))
      let dir = data.split('/')
      dir.pop()
      let files = await utils.ls(path.join(DOWNLOADS_DIR, dir.join('/')))
      broadcast("list", files)
    } catch(error) {
      broadcast("failed", {
        type: "remove",
        error: `Failed to remove "${data}"`,
        trace: error,
        date: new Date(),
      })
    }
  })  

  socket.on("disconnect", function () {
    delete sockets[socket.id]
    console.log("disconnected")
  })
}

fastify.listen(3000, "0.0.0.0", (err, address) => {
  if (err) throw err
  fastify.log.info(`server listening on ${address}`)
})
