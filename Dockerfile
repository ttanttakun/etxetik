FROM accetto/ubuntu-vnc-xfce

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    jackd jack-tools qjackctl pulseaudio-module-jack ffmpeg firefox liquidsoap liquidsoap-plugin-all

# liquidsoap ogg/icecast stream fails if this plugin is installed
RUN apt-get remove -y liquidsoap-plugin-opus

RUN curl -sL https://deb.nodesource.com/setup_13.x | bash - \
    && apt-get install -y nodejs \
    && wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/bin/youtube-dl \
    && chmod +x /usr/bin/youtube-dl

COPY scripts/ /scripts
COPY ctl /home/headless/ctl
COPY ctl-ui /home/headless/ctl-ui



RUN groupadd -r pptruser \
    && useradd -r -g pptruser -G audio,video pptruser \
    && mkdir -p /home/headless/.config/rncbc.org \
    && mkdir -p /home/headless/.config/autostart \
    && cp /scripts/QjackCtl.conf /home/headless/.config/rncbc.org/ \
    && cp /scripts/jacknfirefox.desktop /home/headless/.config/autostart/ \
    && cp -r /scripts/firefox_profile /home/headless/firefox_profile \
    && chown -R pptruser:pptruser /home/headless \
    && npm i -g pm2

USER pptruser

RUN cd /home/headless/ctl && npm i \
    && cd /home/headless/ctl-ui && npm i && npm run build && cp -r dist /home/headless/ctl/public



