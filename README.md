# etxetik

Zuzeneko irratsaioa etxetik egiteko *sistema* librea.

![etxetik-pantaila-argazkia](./img/etxetik.png)

## nola?

Jitsi bidez egiten da irratsaioa. **etxetik** sistema Jitsi gelara sartu (beste erabiltzaile bat bezala), audioa jaso eta jario bat sortzen du beste audio-erreproduzitzaile batekin entzun ahal dena. 

Era beran audio-erreproduzitzaile partekatu baten funtzionaltasuna du. Honela, erabiltzaile bat baino gehiagok kudeatu dezake sistema aldi berean. Besteak beste deskargak (youtube-dl erabiltzen da, beraz aukera ugari dago), erreprodukzio ilara, bolumena, etab.

Musika/audioa Jitsi gelan txertatuko da, hizlariek entzun dezaten. Jitsitik jasotzen den audioari (ahotsa bakarrik) musika hari hau txertatu eta ondoren sortzen da kanporako audio jarioa. Honela sortuko den azken jario honetan musika Jitsitik jasoko balitza baino kalitate hobean entzuten da.

## teknologia

* Docker
* Firefox
* Liquidsoap
* JACK + qjackctl + pulseaudio-module-jack + PulseAudio
* nodejs
* Vue
* WebSockets (socket.io)
* youtube-dl
* FFmpeg

Azpitik *headless* ubuntu bat erabiltzen da (https://github.com/accetto/ubuntu-vnc-xfce) Firefox audio eta guzti exekutatu ahal izateko.

## kodea

`ctl` karpetan zerbitzariaren kodea dago eta `ctl-ui`-n Vue bezeroarena.

## martxan jarri

Docker eta docker-compose behar dira. `docker-compose.yml` fitxategian pasahitzak aldatu (`icescast_iturri_pasahitza` bi aldiz dago, aldatzean berdina jarri bi kasuetan).

Exekutatu `docker-compose up -d`.

`http://ZERBITZARI_HELBIDEA`: kudeatzailea, `CTL_PW`-ren balioa da pasahitza.

`http://ZERBITZARI_HELBIDEA:8000`: Icecast. 

Jarioaren helbidea `http://ZERBITZARI_HELBIDEA:8000/ICECAST_MOUNT` (ICECAST_MOUNT aldatu ez bada).

## kudeatzailea

Sartu eta berehala pasahitza eskatzen du. Gaizki sartzekotan freskatu orria (F5). Behean ezkerretan connected/disconnected jartzen du konektatuta zauden ala ez jakiteko.

DOWNLOAD botoia: sakatu eta irekiko den lehioan youtube bideo baten helbidea (`https://www.youtube.com/watch?v=xxxxxxx` bakarrik) sartu eta ADOS/OK eman. Bideoaren audioa deskargatu eta erabilgarri egongo da CATALOG atalean. Leku ezberdin ugaritatik deskargatu daitezke (soundcloud, zuzenean eskuragarri dauden mp3-ak...)

Goian eskubitan bolumen kontrola dago eta honen azpian TALKOVER botoia dago. Hau sakatuta bolumena 0.1-ean jarriko da, honela lasai hitzegin ahal izateko. STOP TALKOVER sakatuta lehenoko bolumenera itzuliko da.

TALKOVER botoiaren ezkerretan Jitsi gela ireki/aldatu/ixteko botoiak daude. OPEN JITSI ROOM sakatuta lehiotxo bat irekiko da, hemen gelaren izena sartu eta ADOS/OK eman. Segundu gutxiren bueltan sartuko da *sistema* Jitsi gela hortan. Aldatu nahi bada sakatu CHANGE JITSI ROOM. Amaitzean sakatu CLOSE JITSI ROOM.

CATALOG atalean fitxategiek lau botoi dituzte:
    * `d` : ezabatu
    * `r` : berrizendatu
    * `>` : lehio berri batean entzun (zerrendara gehitu gabe)
    * `+` : ilarari gehitu

`Filter catalog...` jartzen duen lekua testu eremu bat da, katalogoa iragazteko.

QUEUEn ilara azaltzen da. Botoi bakarra dago (`-`), ilaratik kentzeko.

PLAYED da azken aldian entzun direnen zerrenda. `clear` emanda garbitu daiteke.

> Aginduak ematean emaitza ez da berehalakoa, beraz botoiren bat klikatu eta gero eman segundu pare bat birritan sakatu ordez!
