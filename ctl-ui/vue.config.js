module.exports = {
  chainWebpack: config => {
    config.module
      .rule('pug')
      .use('pug-plain-loader')
        .loader('pug-plain-loader')
        .tap(options => {
          return options
        })
  },
  css: {
    sourceMap: true
  }
}